package twitterbot

import (
	"fmt"
	"gitlab.com/pvormste/never-forget-article13-bot/election"
	"strings"
	"time"
)

func ElectionReminder(election election.Election) (tweet string, characters int, isTweetable bool) {
	remainingDays := int(election.Date.Sub(time.Now()).Hours() / 24)
	if remainingDays < 0 {
		return "", 0, false
	}

	tags := strings.Join(election.Hashtags, " ")
	tweet = fmt.Sprintf("ServiceTweet:\nNoch %v Tage bis zur %s\n%s", remainingDays, election.Name, tags)

	characters = len(tweet)
	isTweetable = characters <= CharacterLimit

	return tweet, characters, isTweetable
}
