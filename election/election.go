package election

import (
	"time"
)

type Election struct {
	Name     string
	Date     time.Time
	Hashtags []string
}

func NextElection() Election {
	return Election{
		Name: "Wahl zum Europäischen Parlament",
		Date: time.Date(2019, 5, 26, 0, 0, 0, 0, time.UTC),
		Hashtags: []string{
			"#NieWiederCDU",
			"#WählenGehen",
		},
	}
}
