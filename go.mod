module gitlab.com/pvormste/never-forget-article13-bot

go 1.12

require (
	github.com/cenkalti/backoff v2.1.1+incompatible // indirect
	github.com/dghubble/go-twitter v0.0.0-20190305084156-0022a70e9bee // indirect
	github.com/dghubble/sling v1.2.0 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
)
